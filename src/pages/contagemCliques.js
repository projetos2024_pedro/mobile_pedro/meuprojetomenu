import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, Button, TouchableOpacity, ScrollView } from 'react-native';
import {useEffect, useState} from 'react';

function ContagemCliques ({navigation}) {
    const [count, setCount] = useState(0);

    return (
      <View style={styles.container}>
          <Text style={styles.textContagem}>CONTAGEM DE CLIQUES ({count})</Text>
          <TouchableOpacity style={styles.button} onPress={() => setCount(count + 1)}
      >
        <Text style={styles.textClique} >CLIQUE</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.back} onPress={() => navigation.goBack()}>
        <Text style={styles.textButton}>Voltar para o menu</Text>
        </TouchableOpacity>
      </View>
      )
    }

    export default ContagemCliques;

    const styles = StyleSheet.create({
        container: {
          flex: 1,
          flexDirection: 'column',
          justifyContent: 'space-around',
          alignItems: 'center',
        },
        text: {
            color: "#05C7F2",
            fontSize: 4,
          },
          textContagem: {
            top: 80,
            fontWeight: 'bold'
          },
          button: {
            backgroundColor: '#04BF8A',
            padding: 6,
            borderRadius: 7,
          },
          textClique: {
            color: '#fff',
            fontWeight: 'bold'
          },
          textButton: {
            color: "#FFFAFA",
            fontWeight: 'bold'
          },  back: {
            backgroundColor: "#088FFE",
            padding: 10,
            borderRadius: 7,
          },
    });